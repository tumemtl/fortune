import React, { useState } from 'react';
import { StaticImage } from 'gatsby-plugin-image';
import { Link } from 'gatsby';

import { AiOutlineSearch, AiOutlineMenu, AiOutlineClose } from 'react-icons/ai';

export default function index() {
  const NavigationStyling = `relative w-full lg:w-max inline-block lg:mx-[1.4rem] border-b-[1px] border-lightGray border-lightGray  text-[12px] lg:text-[16px] lg:p-0 before:absolute before:right-0 before:bottom-0 lg:before:h-[4px] before:w-[0%] before:bg-theme before:transition-width hover:before:left-0 hover:before:w-[100%] lg:border-0`;
  const ActiveNavigationStyling = `lg:inline-block relative before:absolute before:bg-orange lg:before:h-[4px] before:w-[100%] before:right-0 before:bottom-0 hover:before:w-[100%] before:transition-width hover:before:left-0
  `;

  let [open, setOpen] = useState(false);
  const changeIcon = (state: any) => {
    if (state == true) {
      return false;
    } else {
      return true;
    }
  };

  let [search, setSearch] = useState(false);
  const changeSearch = (state: any) => {
    if (state == true) {
      return false;
    } else {
      return true;
    }
  };

  return (
    <div
      className={`mx-auto h-full w-screen shadow-[0_35px_20px_-15px_#cfcfcf4a]`}
    >
      <div className="absolute w-screen bg-white lg:relative lg:flex lg:justify-between xl:container">
        {/* Homepage */}
        <div className="fixed z-30 lg:relative">
          <div className="absolute lg:relative">
            <div className="flex w-screen justify-between bg-white px-[5%] shadow-[0_10px_30px_0px_#cfcfcf4a] lg:block lg:w-fit lg:px-0 lg:shadow-[0_0px_0px_0px_#cfcfcf4a]">
              <Link to="/">
                <StaticImage
                  src="../../images/fortune.png"
                  alt=""
                  // width={190}
                  className="h-full lg:w-[144px]"
                  placeholder="none"
                />
              </Link>

              <button
                className="transition-all my-auto duration-300 ease-in-out lg:hidden"
                onClick={() => {
                  setOpen((old) => changeIcon(old));
                }}
              >
                {open === false ? (
                  <div className="flex">
                    <span className="h-[2rem] border-l-[1px] border-lightGray stroke-[1px]"></span>
                    <AiOutlineMenu
                      size={25}
                      className="my-auto ml-[2rem]"
                      color="lightGray"
                    />
                  </div>
                ) : (
                  <AiOutlineClose size={25} color="gray" />
                )}
              </button>
            </div>
          </div>
        </div>
        <div
          className={`fixed z-40 my-auto h-full w-screen bg-white  pt-[70px] duration-300 ease-in-out lg:relative lg:grid lg:h-fit lg:grid-cols-2 lg:pt-0 ${
            open ? 'left-0' : '-left-[100%] lg:left-0'
          }`}
        >
          <button
            className="transition-all absolute top-[2rem] right-[1.5rem] my-auto border-lightGray duration-300 ease-in-out lg:hidden"
            onClick={() => {
              setOpen((old) => changeIcon(old));
            }}
          >
            {open === false ? (
              <AiOutlineMenu size={25} className="stroke-[1px]" color="gray" />
            ) : (
              <AiOutlineClose size={25} color="gray" />
            )}
          </button>
          {/* Navigation */}
          <div className="grid grid-rows-6 p-[1.5rem] lg:flex lg:p-0 lg:pl-[4rem]">
            <div className="my-auto">
              <Link
                to="/"
                className={NavigationStyling}
                activeClassName={ActiveNavigationStyling}
              >
                <p className="border-t-[1px] border-lightGray px-[16px] py-[12px] text-[16px] font-[500] lg:border-0 lg:p-0">
                  Home
                </p>
              </Link>
            </div>
            <div className="my-auto">
              <Link
                to="/our-business/"
                className={NavigationStyling}
                activeClassName={ActiveNavigationStyling}
              >
                <p className="px-[16px] py-[12px] text-[16px] font-[500] lg:p-0">
                  Our Business
                </p>
              </Link>
            </div>
            <div className="my-auto">
              <Link
                to="/who-we-are/"
                className={NavigationStyling}
                activeClassName={ActiveNavigationStyling}
              >
                <p className="px-[16px] py-[12px] text-[16px] font-[500] lg:p-0">
                  Who We Are
                </p>
              </Link>
            </div>
            <div className="my-auto">
              <Link
                to="/news/"
                className={NavigationStyling}
                activeClassName={ActiveNavigationStyling}
              >
                <p className="px-[16px] py-[12px] text-[16px] font-[500] lg:p-0">
                  News
                </p>
              </Link>
            </div>
            <div className="my-auto">
              <Link
                to="/contact-us/"
                className={NavigationStyling}
                activeClassName={ActiveNavigationStyling}
              >
                <p className="px-[16px] py-[12px] text-[16px] font-[500] lg:p-0">
                  Contact Us
                </p>
              </Link>
            </div>
          </div>

          {/* Search Button */}
          <div className="right-0 z-30 col-end-4 my-auto max-w-max p-[10px] pr-[1rem] lg:border-r-[1px] lg:border-lightGray">
            <button
              onClick={() => {
                setSearch((old) => changeSearch(old));
              }}
              className="relative my-auto hidden lg:block"
            >
              {search === false ? (
                <AiOutlineSearch
                  size={30}
                  className="h-full w-full duration-200 ease-in-out hover:fill-black"
                  color="lightGray"
                />
              ) : (
                <AiOutlineClose size={30} color="gray" />
              )}
            </button>
            <div
              className={`absolute right-[50%] top-[42%] my-auto flex translate-x-[50%] border-[1px] border-lightGray bg-white  p-[0.8rem] lg:right-[1rem] lg:top-[4rem] lg:flex lg:translate-x-0 lg:rounded-lg lg:border-0 lg:border-gray lg:shadow-[0px_0px_25px_0px_#cfcfcf4a] lg:${
                search ? 'block' : 'hidden'
              }`}
            >
              <input
                className="outline-0"
                placeholder="Search..."
                type="text"
              ></input>

              <button className="">
                <AiOutlineSearch
                  size={30}
                  className="hover:fill-black"
                  color="gray"
                />
              </button>
              {/* </div> */}
            </div>
          </div>
        </div>

        {/* Hamburger menu */}

        {/* <div className="lg:hidden absolute">
        <div className="w-screen h-screen bg-white p-[1rem] relative z-20">
          <div className="flex w-full justify-between">
            <div></div>

            <button className="">
              <AiOutlineClose size={30} />
            </button>
          </div>
        </div>
      </div> */}
      </div>
    </div>
  );
}
