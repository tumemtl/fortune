import React from 'react';

import Slider from './Slider';

export default function OurBusiness() {
  return (
    <div className="xl:container">
      {/* Title */}
      <div className="relative">
        <span className="absolute top-[50%] h-[32px] -translate-y-[50%] border-l-[4px] border-theme"></span>
        <h2 className="pl-[1rem] text-left text-[32px] font-[300] text-gray">
          Our business
        </h2>
      </div>

      <Slider />
    </div>
  );
}
