import React from 'react';
import SwiperCore, { EffectCoverflow, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';

import { AiOutlineCi } from 'react-icons/ai';

export default function Slider() {
  SwiperCore.use([EffectCoverflow, Pagination]);
  const CartStyling = `w-full border-[1px] border-lightGray p-[48px] text-center duration-300 ease-in-out hover:-translate-y-[1rem]`;

  return (
    <div>
      <Swiper
        breakpoints={{
          // when window width is >= 640px
          0: {
            slidesPerView: 1,
          },
          1024: {
            // width: 800,
            slidesPerView: 3,
          },
          // when window width is >= 768px
          1300: {
            // width: 768,
            slidesPerView: 4,
          },
        }}
        spaceBetween={70}
        onSlideChange={() => console.log('slide change')}
        onSwiper={(swiper) => console.log(swiper)}
      >
        <SwiperSlide>
          <div className={CartStyling}>
            <AiOutlineCi size={100} className="mx-auto fill-theme" />
            <h1 className="mt-[16px] mb-[12px] text-[24px] font-[700]">
              Mellurgical coal
            </h1>
            <ul className="list-disc text-left leading-[2rem]">
              <li>
                <p className="text-gray">Coking coal</p>
              </li>
            </ul>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={CartStyling}>
            <AiOutlineCi size={100} className="mx-auto fill-theme" />
            <h1 className="mt-[16px] mb-[12px] text-center text-[24px] font-[700]">
              Metals
            </h1>
            <ul className="list-disc text-left leading-[2rem]">
              <li>
                <p className="text-gray">Copper cathode</p>
              </li>
              <li>
                <p className="text-gray">Copper powder</p>
              </li>
              <li>
                <p className="text-gray">Gold concentrate</p>
              </li>
              <li>
                <p className="text-gray">Metal ores</p>
              </li>
            </ul>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={CartStyling}>
            <AiOutlineCi size={100} className="mx-auto fill-theme" />
            <h1 className="mt-[16px] mb-[12px] text-center text-[24px] font-[700]">
              Metals
            </h1>
            <ul className="list-disc leading-[2rem]">
              <li>
                <p className="leading-0 text-left text-gray">
                  Fluorspar and acid spar
                </p>
              </li>
            </ul>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={CartStyling}>
            <AiOutlineCi size={100} className="mx-auto fill-theme" />
            <h1 className="mt-[16px] mb-[12px] text-center text-[24px] font-[700]">
              Metals
            </h1>
            <ul className="list-disc text-left leading-[2rem] ">
              <li>
                <p className="text-gray">Railway</p>
              </li>
              <li>
                <p className="text-gray">Trucking</p>
              </li>
            </ul>
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className={CartStyling}>
            <AiOutlineCi size={100} className="mx-auto fill-theme" />
            <h1 className="mt-[16px] mb-[12px] text-center text-[24px] font-[700]">
              Metals
            </h1>
            <ul className="list-disc text-left leading-[2rem] ">
              <li>
                <p className="text-gray">Railway</p>
              </li>
              <li>
                <p className="text-gray">Trucking</p>
              </li>
            </ul>
          </div>
        </SwiperSlide>
      </Swiper>
    </div>
  );
}
