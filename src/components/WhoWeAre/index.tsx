import React from 'react';

export default function index() {
  return (

    <div className="px-[5%] lg:px-0 xl:container text-gray">
      <h2  className="text-black">Who we are</h2>
      <p className="mt-[3rem] mb-[1rem] leading-8">
        Established in 2016, Fortune I-Trade PTE Ltd (FIT) is a Singapore
        incorporated company focused on commodity trading. We have vast
        experience in originating copper cathode from Mongolia and selling to
        international markets. FIT has been appointed as Export Management
        Company of Achit Ikht LLC (AIC), the leading Mongolian SX-EW plant for
        copper cathodes. FIT is responsible for handling export operations for
        AIC, such as looking for new potential buyers and dealers; handling
        advertising, marketing and promotions; arranging shipping; and sometimes
        arranging financing.
      </p>
      <p>
        Fortune I-trade Pte Ltd (FIT) is expanding its footprint in the
        commodity trading business, and is now developing metallurgical coal
        platform.
      </p>



    </div>
  );
}
