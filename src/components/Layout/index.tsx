import * as React from 'React';

import Header from '@/components/Header';
import Footer from '@/components/Footer';
import { Helmet } from 'react-helmet';

type Props = {
  children: React.ReactNode;
  title: string;
};

const Layout = ({ children, title = `Title here......` }: Props) => {
  console.log(title);
  return (
    <div className="overflow-hidden">
      <Helmet title={title + ` - Fortune I-Trade`}>
        <link
          rel="icon"
          href="https://fortuneitrade.com/wp-content/uploads/2019/02/cropped-favICON-512x512-1-32x32.png"
        />
      </Helmet>
      <div className="fixed z-20 bg-white">
        <Header />
      </div>
      {/*  */}
      {title === `Home` ? (
        <div className="relative mx-auto h-auto">
          <div className="z-0 mx-auto h-full  translate-y-0  overflow-hidden lg:px-0 ">
            {children}
          </div>
        </div>
      ) : (
        <div className="relative z-0 mx-auto h-full px-[30px] pt-[150px] lg:translate-y-0  lg:pt-[4rem]">
          {children}
        </div>
      )}

      {/* <Footer /> */}
    </div>
  );
};

export default Layout;
