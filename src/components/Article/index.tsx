import React from 'react';
import { StaticImage } from 'gatsby-plugin-image';
import {
  FaFacebookF,
  FaTwitter,
  FaPinterest,
  FaLinkedin,
} from 'react-icons/fa';
import { AiOutlinePlus, AiOutlineSearch } from 'react-icons/ai';

export default function Articles() {
  const SocialStyles = `duration-300 ease-in-out hover:fill-theme mb-[2rem]`;

  return (
    <div className="xl:container">
      <div>
        <h2 className="lg:text-[48px]">News</h2>
      </div>

      <div className="lg:grid lg:grid-cols-[max-content_60%_40%]">
        {/* Socials */}
        <div className="mt-[20rem] hidden p-[2rem] lg:block">
          <a href="">
            <FaFacebookF size={25} className={SocialStyles} />
          </a>
          <a href="">
            <FaTwitter size={25} className={SocialStyles} />
          </a>
          <a href="">
            <FaPinterest size={25} className={SocialStyles} />
          </a>
          <a href="">
            <FaLinkedin size={25} className={SocialStyles} />
          </a>
        </div>

        {/* Article */}
        <div className="mt-[4rem]">
          {/* Date */}
          <div className="relative">
            <span className="absolute top-[50%] h-[24px] -translate-y-[50%] border-l-[4px] border-theme"></span>
            <p className="pl-[1rem] text-left text-[14px] font-[700]">
              POSTED ON SEPTEMBER 10, 2021 IN NEWS
            </p>
          </div>

          {/* Title */}
          <h2 className="mt-[1rem] text-[24px] font-[700] leading-8 lg:text-[48px] lg:leading-[3.5rem]">
            China eyes more coal imports from Mongolia as supply shortage bites
          </h2>

          {/* Mobile Socials */}
          <div className="my-[2rem] flex lg:hidden">
            <a href="">
              <FaFacebookF size={25} className="mr-[1rem]" />
            </a>
            <a href="">
              <FaTwitter size={25} className="mr-[1rem]" />
            </a>
            <a href="">
              <FaPinterest size={25} className="mr-[1rem]" />
            </a>
            <a href="">
              <FaLinkedin size={25} className="mr-[1rem]" />
            </a>
          </div>

          {/* Article */}
          <div className="lg:mt-[3rem]">
            <ul className="list-inside list-square text-[18px] leading-7 text-gray">
              <li className="mb-[1rem]">
                At a meeting on Tuesday, China requested more coal from Mongolia
                to help offset an ongoing supply shortage
              </li>
              <li>
                Mongolia has replaced Australia as China’s largest source of
                imported coking coal since the second half of last year
              </li>
            </ul>

            <StaticImage
              className="my-[2rem]"
              src="https://img.i-scmp.com/cdn-cgi/image/fit=contain,width=1098,format=auto/sites/default/files/styles/1200x800/public/d8/images/methode/2021/09/09/3acdbbda-1090-11ec-aa5f-4ba6b5f6c41c_image_hires_114948.jpeg?itok=pVopnbhK&v=1631159401"
              alt=""
            />

            <div>
              <p className="text-[18px] text-gray">
                Mongolia has replaced Australia to become China’s largest source
                of imported coking coal since the second half of last year.
                Photo: AP <br /> Beijing has stepped up efforts to source more
                coal from neighboring Mongolia amid an ongoing supply shortage,
                raising more questions about how long a ban on Australian coal
                can last.
                <br />
                <br /> China is suffering from a spike in coal prices amid
                declining supplies, which has also prompted authorities to ban
                an influential local trading platform from updating coal prices
                and market news that could fuel speculation. <br />
                <br /> Rising coal prices are another worry for China, which is
                already dealing with high raw material costs that are hurting
                businesses.
                <br />
                <br /> In a virtual meeting with Mongolian deputy prime minister
                Amarsaikhan Sainbuyan on Tuesday, Chinese commerce minister Wang
                Wentao broached the topic of buying more mineral and
                agricultural products from Mongolia, a ministry statement said.
                <br />
                <br />
              </p>

              {/* Source */}
              <p className="text-[18px] text-gray">
                Source:{' '}
                <a
                  className="text-black duration-300 ease-in-out hover:text-gray"
                  href="https://www.scmp.com/economy/china-economy/article/3148035/china-eyes-more-coal-imports-mongolia-supply-shortage-bites"
                >
                  South China Morning Post
                </a>
              </p>
            </div>
          </div>

          {/* Next post */}
          <div className="mt-[1rem] border-t-[1px] border-lightGray pt-[1rem] text-right ">
            <a
              href=""
              className="font-[700] text-gray duration-300 ease-in-out hover:text-black"
            >
              Next post
            </a>
          </div>
        </div>

        {/* Recents */}
        <div className="pt-[96px] lg:px-[7rem]">
          {/* Search */}
          <div className="relative mb-[3rem]">
            <input
              type="text"
              className="w-full border-[1px] border-lightGray p-[1rem] text-[16px] text-gray outline-none duration-300 ease-in-out hover:border-gray"
              placeholder="Search..."
            />
            <button className="absolute right-[1rem] top-[50%] -translate-y-[50%]">
              <AiOutlineSearch size={20} />
            </button>
          </div>

          <div className="relative">
            <p className="text-[18px] font-[600]">Recent posts</p>
          </div>

          {/* Articles */}
          <div className="grid grid-cols-[30%_70%] gap-3 bg-white p-[1rem] shadow-lg">
            <div className="relative h-max">
              <a href="relative group">
                <div className="group-hover:opacity-1 opacity-0">
                  <span className="absolute top-0 bottom-0 right-0 left-0 z-10 h-full w-full bg-black opacity-80"></span>
                  <AiOutlinePlus
                    color="white"
                    className="absolute left-[50%] top-[50%] z-20 -translate-x-[50%] -translate-y-[50%] opacity-100"
                  />
                </div>
                <StaticImage
                  src="https://fortuneitrade.com/wp-content/uploads/2021/07/bull-market-1024x576-1-150x150.jpeg"
                  alt=""
                  className=""
                />
              </a>
            </div>
            <div className="">
              {/* Title */}
              <a
                href=""
                className="font-[700] duration-300 ease-in-out hover:text-theme"
              >
                China eyes more coal imports from Mongolia as supply shortage
                bites
              </a>

              {/* Date */}
              <p className="mt-[0.5rem] text-[16px] text-gray">July 20, 2021</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
