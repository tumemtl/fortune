import * as React from 'react';
import { StaticImage } from 'gatsby-plugin-image';
const News = () => {
  return (
    <section className="h-auto max-w-[413px] bg-white px-[20px] pb-[40px]">
      <div className="shadow-3xl">
        <StaticImage src="../../images/bull-market.jpeg" alt="" />
        <div className="px-[40px] pb-[48px] pt-[40px]">
          <h3 className="relative mb-2 border-l-4 border-theme pl-[12px] text-[14px] font-[800] uppercase leading-[26px] tracking-[0.5px] text-dark">
            {' '}
            News / September, 23, 2021
          </h3>
          <a className="text-[24px] font-[700] text-dark transition-colors duration-300 ease-out hover:text-theme">
            Could improved railway connectivity to Russia be the answer to the
            Tianjin container crisis?
          </a>
          <p className="mt-2 text-[16px] font-[600] text-darkGray">
            EuroChamber’s second webinar on “Logistics &amp; Connectivity with
            Europe” was held on June 29. [...]
          </p>
          <button className=" relative mt-6 text-dark duration-500 before:absolute before:right-0 before:bottom-0 before:h-[4px] before:w-full before:bg-theme before:transition-width hover:before:left-0 hover:before:w-0 ">
            Read more
          </button>
        </div>
      </div>
    </section>
  );
};

export default News;
