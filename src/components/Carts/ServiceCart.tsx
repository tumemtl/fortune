import * as React from 'react';
import { StaticImage } from 'gatsby-plugin-image';

const ServiceCart = () => {
  return (
    <div
      className=" group relative h-fit w-[300px]  bg-white p-[48px] transition-all duration-300 ease-out hover:bg-theme hover:pb-4 hover:shadow-3xl
      "
    >
      <div className="  w-full duration-300 group-hover:-translate-y-4">
        <StaticImage
          src="../../images/background/icon.png"
          alt=""
          width={88}
          className="mb-4"
        />
        <h3 className="font-600 mb-3 mt-4 text-2xl text-dark">
          Metallurgical Coal
        </h3>
        <ul className="cursor-pointer  list-inside list-disc group-hover:text-darkGray [&>*]:py-0.5 [&>*]:leading-[32px]">
          <li>Copper cathode</li>
          <li>Copper powder</li>
          <li>Gold concentrate</li>
          <li>Metal ores</li>
        </ul>
      </div>
    </div>
  );
};

export default ServiceCart;
