import React from 'react';
import { Link } from 'gatsby';

import { AiOutlineMail } from 'react-icons/ai';

export default function Footer() {
  return (
    <div className="relative bottom-0">
      <div className="flex w-screen justify-between bg-black p-[2rem] px-[4rem]">
        <h1 className=" my-auto text-gray">
          Copyright © Fortune I-Trade PTE LTD. 2021
        </h1>
        <a href="mailto:info@fortuneitrade.com">
          <AiOutlineMail
            color="gray"
            className="duration-200 ease-in-out hover:fill-white"
            size={27}
          />
        </a>
      </div>
    </div>
  );
}
