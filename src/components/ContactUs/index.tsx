import React from 'react';
import { AiOutlineMail } from 'react-icons/ai';

export default function index() {
  const InputStyling = `border-[1px] mb-[1rem] border-lightGray p-[1rem] outline-none duration-300 ease-in-out focus:border-black`;

  return (
    <div className="xl:container">
      <h2 className="mb-[4rem] text-left">Contact us</h2>

      <div className="lg:grid lg:grid-cols-3 lg:gap-[3rem]">
        <div className="col-span-2 mb-[4rem] flex flex-col lg:mb-0">
          <input type="text" placeholder="Your Name" className={InputStyling} />
          <input
            type="text"
            placeholder="Your Email"
            className={InputStyling}
          />
          <input
            type="text"
            placeholder="Your subject"
            className={InputStyling}
          />
          <textarea
            name="Additional information"
            id=""
            cols={30}
            rows={3}
            placeholder="Additional information"
            className={InputStyling}
          ></textarea>

          <button className="max-w-max bg-theme py-[1rem] px-[2rem] font-[600] duration-300 ease-in-out hover:bg-black hover:text-white">
            Send message
          </button>
        </div>
        <div className="">
          <div className="mb-[1rem] border-b-[1px] border-dashed border-lightGray">
            <h1 className="text-[18px] font-[600]">Mongolia</h1>
            <p className="my-[1rem] text-gray">
              Regis Place #1001, Chinggis Avenue 33/2, Khoroo 15, Khan-Uul
              District, Ulaanbaatar
            </p>
          </div>
          <div className="mb-[1rem] border-b-[1px] border-dashed border-lightGray">
            <h1 className="text-[18px] font-[600]">Singapore</h1>
            <p className="my-[1rem] text-gray">
              50 Raffles Place #15-05/06, Singapore Land Tower, Singapore 048623
            </p>
          </div>
          <div className="mb-[1rem] border-b-[1px] border-dashed border-lightGray">
            <h1 className="text-[18px] font-[600]">Telephone</h1>
            <p className="my-[1rem] text-gray">+976-99111495, +976-75331001</p>
            <p className="my-[1rem] text-gray">www.fortuneitrade.com </p>
          </div>
          <div className="border-b-[1px] border-dashed border-lightGray">
            <h1 className="text-[18px] font-[600]">Email</h1>
            <span className="py-[1rem]">
              <p className="my-[1rem]">
                <a href="mailto:info@fortuneitrade.com">
                  <AiOutlineMail
                    color="black"
                    className="bg-theme p-[0.5rem] duration-200 ease-in-out hover:bg-white"
                    size={40}
                  />
                </a>
              </p>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}
