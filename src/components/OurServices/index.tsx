import { StaticImage } from 'gatsby-plugin-image';
import * as React from 'react';
import ServiceCart from '../Carts/ServiceCart';
import gsap, { Power0, Power3, Linear } from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import { MdArrowBackIosNew } from 'react-icons/md';
import News from '../Carts/NewsCart';
gsap.registerPlugin(ScrollTrigger);

const OurServices = () => {
  // gsap
  //   .timeline({
  //     scrollTrigger: {
  //       trigger: '.container__parallax',
  //       start: 'top center',
  //       endTrigger: 'top 100px',
  //       end: 'bottom 50%+=100px',
  //       markers: true,
  //     },
  //   })
  //   .to('.background', {
  //     duration: 5,
  //     y: 10,
  //     ease: Linear.easeNone,
  //   });

  React.useEffect(() => {
    gsap
      .timeline({
        scrollTrigger: {
          trigger: '.container__parallax',
          start: ' bottom',
          endTrigger: 'top',
          scrub: true,
          end: 'bottom 50%+=200px',
          markers: true,
        },
      })

      .to('.background', {
        y: 400,
        duration: 3,
        ease: Linear.easeNone,
      });
  }, []);
  return (
    <div className="">
      <div className="relative mx-auto h-auto">
        <div className=" absolute top-0 left-[50%] z-0 min-h-fit w-screen  -translate-x-[50%]  xl:w-[1280px]  ">
          <StaticImage
            src="../../images/background/map_transparent_dark.png"
            alt=""
            className=""
            layout="fullWidth"
          />
        </div>
        <div className="z-5 absolute top-0 left-0 h-[25%] w-screen bg-gradient-to-b from-white to-transparent"></div>

        <section className="z-1 mx-auto px-[5%] pt-[150px] lg:container lg:px-0 lg:pt-[72px] lg:pb-[96px]">
          <h3 className="relative mb-6 border-l-4 border-theme pl-[12px] text-[14px] font-[800] uppercase leading-[26px] tracking-[0.5px] text-dark">
            Our services
          </h3>
          <div className="flex ">
            <ServiceCart />
            <ServiceCart />
            <ServiceCart />
            <ServiceCart />
          </div>
        </section>
      </div>
      <div className="container__parallax relative mt-8  h-[290px] w-screen max-w-none overflow-hidden ">
        <StaticImage
          src="../../images/background/parallaxBackground.jpeg"
          alt=""
          className="background absolute top-[50%] left-[50%] -translate-y-[50%] -translate-x-[50%]"
          layout="fixed"
        />
      </div>

      <section className="mt-24 grid grid-cols-1 gap-y-10 px-[5%] lg:px-0 xl:container ">
        <div className="flex justify-between">
          <h3 className="relative mb-6  border-l-4 border-theme pl-[12px] text-[14px] font-[800] uppercase leading-[26px] tracking-[0.5px] text-dark">
            Latest News
          </h3>
          <button className="relative mx-auto mr-48 min-w-[200px]  bg-secondaryDark pt-[12px] pb-[13px] before:absolute before:right-0 before:bottom-0 before:h-full  before:w-0  before:bg-black before:transition-width  before:duration-700 before:ease-in-expo hover:before:left-0 hover:before:w-full hover:after:w-0 hover:after:transition-width ">
            <span className="relative  flex w-auto items-center justify-center  text-[16px] font-[600] text-white">
              More news{' '}
              <span className="bottom-0 ml-1 translate-y-[1.5px] text-center text-[9px] font-[800]">
                <MdArrowBackIosNew className="rotate-180" />
              </span>
            </span>
          </button>
        </div>
        <div className="flex ">
          <News />
          <News />
          <News />
        </div>
      </section>
    </div>
  );
};
export default OurServices;
