import * as React from 'react';
import Layout from '@/components/Layout';
import WhoWeAre from '@/components/WhoWeAre';
import '@/styles/global.css';
import gsap, { Linear } from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import { StaticImage } from 'gatsby-plugin-image';
export default function Home() {
  React.useEffect(() => {
    gsap
      .timeline({
        scrollTrigger: {
          trigger: '.container__parallax',
          start: ' bottom',
          endTrigger: 'top',
          scrub: true,
          end: 'bottom 50%+=200px',
          markers: true,
        },
      })
      .to('.background', {
        y: 400,
        duration: 3,
        ease: Linear.easeNone,
      });
  }, []);
  return (
    <main>
      <Layout title="Who we are">
        <WhoWeAre />
        <div className="container__parallax relative mt-8  h-[290px] w-screen max-w-none overflow-hidden ">
          <StaticImage
            src="../../images/background/parallaxBackground.jpeg"
            alt=""
            className="background absolute top-[50%] left-[50%] -translate-y-[50%] -translate-x-[50%]"
            layout="fixed"
          />
        </div>
        <div className="h-[25vw] w-full "></div>
      </Layout>
    </main>
  );
}
