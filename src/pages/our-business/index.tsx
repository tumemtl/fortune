import Layout from '@/components/Layout';
import OurBusiness from '@/components/OurBusiness';
import '@/styles/global.css';

export default function Home() {
  return (
    <main>
      <Layout title="Our Business">
        <div className="min-h-screen">
          <OurBusiness />
        </div>
      </Layout>
    </main>
  );
}
