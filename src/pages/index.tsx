import Layout from '@/components/Layout';
import OurServices from '@/components/OurServices';
import '../styles/global.css';

export default function Home() {
  return (
    <main>
      <Layout title="Home">
        <h1 className="">
          <OurServices />
        </h1>
      </Layout>
    </main>
  );
}

import React, { useEffect, useState } from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import apiPosts from '@/api/posts';

const IndexPage = () => {
  // const [data, setData] = useState(null);
  // const gatsbyRepoData = useStaticQuery(graphql`
  //   query {
  //     github {
  //       repository(name: "gatsby", owner: "gatsbyjs") {
  //         id
  //         nameWithOwner
  //         url
  //       }
  //     }
  //   }
  // `);

  useEffect(() => {
    apiPosts.find().then((res) => {
      console.log('DATA: ', res);
    });
  }, []);

  return (
    <Layout title="Home">
      <p>
        Build Time Data: Gatsby repo{` `}
        {/* <a href={gatsbyRepoData.github.repository.url}>
          {gatsbyRepoData.github.repository.nameWithOwner}
        </a> */}
      </p>
    </Layout>
  );
};

// export default IndexPage;
