import News from '@/components/Carts/NewsCart';
import Layout from '@/components/Layout';
import '@/styles/global.css';

export default function Home() {
  return (
    <main>
      <Layout title="Fortune">
        <div className="px-[5%] lg:container lg:px-0">
          <section className="">
            <News />
            <News />
            <News />
          </section>
        </div>
      </Layout>
    </main>
  );
}
