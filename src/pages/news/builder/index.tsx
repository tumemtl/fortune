import React from 'react';
import Article from '@/components/Article';
import Layout from '@/components/Layout';

export default function News() {
  return (
    <Layout
      title="China eyes more coal imports from Mongolia as supply shortage bites
    "
    >
      <Article />
    </Layout>
  );
}
