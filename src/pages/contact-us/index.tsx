import Layout from '@/components/Layout';
import ContactUs from '@/components/ContactUs';
import '@/styles/global.css';

export default function Home() {
  return (
    <main>
      <Layout title="Contact Us">
        <ContactUs />
      </Layout>
    </main>
  );
}
