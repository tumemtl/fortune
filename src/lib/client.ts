import axios from 'axios';

const client = axios.create({
  baseURL: process.env.GATSBY_API_UR,
});

export default client;
