export const GET_ALL_POSTS = `
    query GetPosts {
        posts {
            id
            title
        }
    }
`;
