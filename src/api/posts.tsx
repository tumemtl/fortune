import client from '@/lib/client';
import { GET_ALL_POSTS } from '@/queries/Post';

const apiPosts = {
  find: () => {
    return client.post('', {
      query: GET_ALL_POSTS,
      variables: {},
    });
  },
};

export default apiPosts;
