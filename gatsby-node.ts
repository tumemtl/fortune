import type { GatsbyNode } from 'gatsby';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';

export const onCreateWebpackConfig: GatsbyNode['onCreateWebpackConfig'] = ({
  actions,
}) => {
  actions.setWebpackConfig({
    resolve: {
      plugins: [new TsconfigPathsPlugin()],
    },
  });
};

// const fetch = (...args) =>
//   import(`node-fetch`).then(({ default: fetch }) => fetch(...args));

// const TOKEN = '';

// exports.sourceNodes = async ({
//   actions: { createNode },
//   createContentDigest,
// }) => {
//   // get data from GitHub API at build time
//   const result = await fetch(`http://localhost:3000/api/graphql`);
//   const resultData = await result.json();
//   // create node for build time data example in the docs
//   createNode({
//     // nameWithOwner and url are arbitrary fields from the data
//     nameWithOwner: resultData.full_name,
//     url: resultData.html_url,
//     // required fields
//     id: `cms-data`,
//     parent: null,
//     children: [],
//     internal: {
//       type: `CMS`,
//       contentDigest: createContentDigest(resultData),
//     },
//   });
// };
