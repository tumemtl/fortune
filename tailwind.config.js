/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        './src/pages/**/*.{js,jsx,ts,tsx}',
        './src/components/**/*.{js,jsx,ts,tsx}',
    ],


    theme: {
        extend: {
            transitionTimingFunction: {
                'in-expo': 'cubic-bezier(.19,1,.22,1)',
            },
            dropShadow: {
                cart: '0px 8px 24px 0px rgb(0, 0, 0 / 8%)',
            },
            boxShadow: {
                '3xl': '0px 8px 24px 0px rgb(0 0 0 / 8%)',
            },
            colors: {
                darkGray: '#616161',
                theme: '#48a1e0',
                gray: `#9a9a9a`,
                lightGray: `#d1d1d1`,
                black: `#131313`,
                dark: '#212121',
                secondaryDark: '#232323',
            },

            fontFamily: {
                main: ['Nunito Sans', 'sans-serif'],
                roboto: ['Roboto', 'sans-serif'],
            },
            fontSize: {
                xs: '.75rem',
                sm: '.875rem',
                tiny: '.875rem',
                base: '1rem',
                lg: '1.125rem',
                xl: '1.25rem',
                '2xl': '1.5rem',
                '3xl': '1.875rem',
                '4xl': '2.25rem',
                '5xl': '3rem',
                '6xl': '4rem',
                '7xl': '5rem',
            },
            lineHeight: {
                mobile: '30px',
                12: '3rem',
            },
            transitionProperty: {
                width: 'width',
            },
        },
        container: {
            center: true,

            screens: {
                '2xl': '1200px',
            },
        },

        listStyleType: {
            none: 'none',
            disc: 'disc',
            decimal: 'decimal',
            square: 'square',
            roman: 'upper-roman',
        },


        fontSize: {
            xs: '.75rem',
            sm: '.875rem',
            tiny: '.875rem',
            base: '1rem',
            lg: '1.125rem',
            xl: '1.25rem',
            '2xl': '1.5rem',
            '3xl': '1.875rem',
            '4xl': '2.25rem',
            '5xl': '3rem',
            '6xl': '4rem',
            '7xl': '5rem',
        },

        transitionProperty: {
            width: 'width',
        },

    },
    plugins: [],
};